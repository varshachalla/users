import { AppBar, Toolbar, Typography } from "@material-ui/core/";
import React, {Fragment} from "react";

import UsersTable from "../containers/UsersTable";

const Layout = classes => (
  <Fragment>
    <AppBar position="absolute">
      <Toolbar>
        <Typography variant="title" color="inherit" noWrap>
          Users
        </Typography>
      </Toolbar>
    </AppBar>
    <UsersTable />
  </Fragment>
);

export default Layout;
